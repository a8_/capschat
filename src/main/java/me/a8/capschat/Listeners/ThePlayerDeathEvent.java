package me.a8.capschat.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;


public class ThePlayerDeathEvent implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (event.getEntity() instanceof Player) {

            if (event.getEntity().getKiller() instanceof Player) {
                Player killer = event.getEntity().getKiller();
                Player player = event.getEntity();

                String capsname = player.getDisplayName().toUpperCase();
                String killercapsname = killer.getDisplayName().toUpperCase();

                killer.sendMessage("§cYOU HAVE KILLED " + capsname);
                player.sendMessage("§cYOU HAVE BEEN KILLED BY " + killercapsname);

                event.setDeathMessage("§c" + killer.getDisplayName().toUpperCase() + " HAS KILLED " + player.getDisplayName().toUpperCase());
            }else{
                event.setDeathMessage("§c" + event.getEntity().getDisplayName().toUpperCase() + " HAS DIED");
            }


        }

    }
}