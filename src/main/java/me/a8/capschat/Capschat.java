package me.a8.capschat;

import me.a8.capschat.Listeners.PlayerChatEvent;
import me.a8.capschat.Listeners.ThePlayerDeathEvent;
import me.a8.capschat.Listeners.ThePlayerJoinEvent;
import me.a8.capschat.Listeners.ThePlayerLeaveEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public final class Capschat extends JavaPlugin {
    //TODO: /brodcast
    //TODO: block /me to bypass makeing chat capital
//TODO: OP MESSAGE
    //TODO: DM'S
    //TODO: diffaculty changes


    @Override
    public void onEnable() {
        System.out.println("CAPS CHAT IS LOADING");
        PluginManager pm = getServer().getPluginManager();
       PlayerChatEvent PlayerChatEventlistener = new PlayerChatEvent();
       pm.registerEvents(PlayerChatEventlistener, this);

        ThePlayerJoinEvent ThePlayerJoinEventlistener = new ThePlayerJoinEvent();
        pm.registerEvents(ThePlayerJoinEventlistener, this);

        ThePlayerLeaveEvent thePlayerLeaveEventlistener = new ThePlayerLeaveEvent();
        pm.registerEvents(thePlayerLeaveEventlistener, this);

        ThePlayerDeathEvent thePlayerDeathEventListener = new ThePlayerDeathEvent();
        pm.registerEvents(thePlayerDeathEventListener, this);
        System.out.println("CAPS CHAT HAS LOADED");
    }

    @Override
    public void onDisable() {
        System.out.println("CAPS CHAT IS SHUTTING DOWN");
    }
}
